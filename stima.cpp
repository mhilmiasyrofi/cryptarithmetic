/*
  Nama  : Muhammad Hilmi Asyrofi
  NIM   : 13515083
  Judul : Cryptarithms
*/

#include <iostream>
#include <fstream>
#include <cstring>
#include <math.h>
#include <vector>
#include <map>
#include <set>

using namespace std;

bool cekUnik(long long n, int p){

  //KAMUS LOKAL
  set<int> digit;
  int count;
  // long long x;

  //ALGORITMA
  if (n == 0)
    return true;

  count = 0;
  while(n>=1){
    digit.insert(n%10);
    count++;
    n /= 10;
  }

  return (count == digit.size());
}

int main(){
  //Untuk menghitung running time
  clock_t tStart = clock();

  //KAMUS
  ifstream in_stream;
  ofstream out_stream;
  vector<string> data;
  vector<char> huruf;
  map<char,int> hash;
  string str;
  long long i,sum,sum_data,ceksum;
  int j,k,x,iter,digit;
  bool cek;

  //membuka file
  in_stream.open("input.txt");
  out_stream.open("output.txt");

  //membaca file eksternal
  while (!in_stream.eof()){
    in_stream >> str;
    data.push_back(str);
  }
  data.pop_back();

  //enentukan huruf-huruf alfabet yang terpakai pada operan dan hasil
  //jumlah huruf yang terpakai harus <= 10
  for ( i = 0; i < data.size() ; i++ ){
    for ( j = 0 ; j < data[i].size() ; j++){
      if (!(data[i][j] == '+' || data[i][j] == '-')){
        if (huruf.empty())
          huruf.push_back(data[i][j]);
        else {
          iter = 0;
          while (huruf[iter] != data[i][j] && iter != huruf.size())
            iter++;
          if (iter == huruf.size())
            huruf.push_back(data[i][j]);
        }
      }
    }
  }

  for (i = pow(10,huruf.size()-1); i < pow(10,huruf.size());i++){
    if (cekUnik(i,huruf.size())){
      for (j = 0 ; j < huruf.size() ; j++){
        digit = i / pow(10,j);
        digit %= 10;
        hash.insert(make_pair(huruf[j],digit));
      }

      cek = true;
      sum_data = 0;
      for (j = 0; j < data.size() - 3; j++){
        sum = 0;
        x = data[j].size();
        for (k = 0; k < data[j].size(); k++){
          sum += hash[data[j][k]]*(pow(10,x-k-1));
        }
        if (sum < pow(10,x-1) && pow(10,x-1) != 1)
          cek = false;
        else
          sum_data += sum;
      }

      x = data.size() - 3;
      sum = 0;
      for (k = 0; k < data[x].size()-1; k++){
        sum += hash[data[x][k]]*(pow(10,data[x].size()-1-k-1));
      }
      if (sum < pow(10,data[x].size()-2) && pow(10,data[x].size()-1) != 1)
        cek = false;
      else
        sum_data += sum;

      ceksum = 0;
      x = data.size();
      for (k = 0; k < data[x].size() ; k++){
        ceksum += hash[data[x][k]]*(pow(10,data[x].size()-k-1));
      }
      if (ceksum < pow(10,data[x].size()-1) && pow(10,data[x].size()-1) != 1)
        cek = false;

      if (sum_data == ceksum && cek){
        // out_stream<<i<<endl;
        for (j = 0; j < data.size() ; j++){
          for (k = 0; k < data[j].size();k++){
            if (data[j][k] == '+' || data[j][k] == '-')
              out_stream<<data[j][k];
            else
              out_stream<<hash[data[j][k]];
          }
          out_stream<<endl;
        }
        out_stream<<endl<<endl;

      }
    }

    hash.clear();
  }

  out_stream<<"Waktu yang diperlukan:" <<endl<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<" detik"<<endl;

  //Menutup file
  in_stream.close();
  out_stream.close();

  return 0;
}
