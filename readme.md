# Tugas Kecil I IF2211 Strategi Algoritma - Semester II Tahun 2016/2017


## Penyelesaian *Cryptarithmetic* dengan Algoritma *Brute Force*

<br />

## Author
* **Muhammad Hilmi Asyrofi** - **13515083** - [Facebook](https://www.facebook.com/muhammad.h.asyrofi)

<br />

## Cara Kompilasi

```
make
```
<br />

## Cara Run Program

```
./stima
```
<br />

## Dokumen
klik [*disini*](https://www.facebook.com/muhammad.h.asyrofi) untuk mengunduh

<br />
<br />

## Deskripsi
*Cryptarithmetic* (atau *cryptarithm*)  adalah sebuah *puzzle* penjumlahan di dalam matematika dimana angka diganti dengan huruf. Setiap angka dipresentasikan dengan huruf yang berbeda. Deskripsi permainan ini adalah: diberikan sebuah penjumlahan huruf, kita diminta untuk mencari angka yang merepresentasikan huruf-huruf tersebut.
Berikut ini contoh dan solusi *cryptarithmetic*:
<br/>

<img src="contoh.jpg" width="170px" height="60px" />
<br/>
dengan demikian S = 9, E = 5, N = 6, D = 7, M = 1, O = 0, R = 8, Y = 2
