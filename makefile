CC = g++
OBJ = stima.o
Cryptarithmetic : $(OBJ)
		$(CC) $(OBJ) -o stima
				-rm $(OBJ)
%.o : %.c %.h
.PHONY : clean
clean :
				-rm stima
